# frozen_string_literal: true
require 'date'
require 'minitest/autorun'
require_relative '../lib/multiformatcv'

class MultiformatCVTest < Minitest::Test
  def setup
    data_dir = File.expand_path('../../sample/data', __FILE__)
    output = File.expand_path('../../sample/cv.html', __FILE__)

    @resume = MultiformatCV.generate(
      'format'    => 'html',
      'data_dir'  => data_dir,
      'output'    => output,
    )
  end

  def test_contact_information_is_read
    assert_match @resume.contact.email, @resume.rendered
    assert_match @resume.contact.name,  @resume.rendered
    assert_match @resume.contact.phone, @resume.rendered
    assert_match @resume.contact.title, @resume.rendered
  end

  def test_job_projects_are_read
    assert @resume.jobs.length > 0
    @resume.jobs.each_with_index do |job, i|
      assert_match job.position, @resume.rendered
      assert_match job.company,  @resume.rendered

      # this is specific to sample template
      if i < 2
        job.projects.each do |p|
          assert_match p.summary, @resume.rendered
        end
      end
    end
  end

  def test_personal_projects_are_read
    @resume.personal.projects.each do |p|
      assert_match p.summary, @resume.rendered
    end
  end

  # This test is "rigged", it is interlocked with the test data,
  # so look at that also.

  def test_technologies_have_experience_time
    n = diff_in_months_from_to_now(DateTime.parse('January 2020').to_time)
    # in months
    expected_results = {
      'JavaScript' => 16,
      'Ruby'       => 16,
      'Angular'    => 16,
      'PHP'        => 16,
      'WireShark'  => 12,
      'HelpDesk'   => 12,
    }
    expected_results['Java'] = n
    expected_results['JavaScript'] += n

    expected_results.each do |k, v|
      assert_equal v, @resume.skills[k], "Experience for skill #{ k } do not match"
    end
  end

  private

  # Generic (30-day) month duration in seconds
  ONE_MONTH = 3600 * 24 * 30

  # @param [Time] t
  # @return [Integer] Difference in months between date & present
  def diff_in_months_from_to_now(t)
    ((t - DateTime.now.to_time) / ONE_MONTH).abs.round
  end
end
