require 'rake'
require 'rake/clean'
require 'rake/testtask'
require 'yard'

lib = File.expand_path("lib", __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'multiformatcv/version'

NAME = 'multiformat-cv'
CLEAN.include ["#{NAME}-*.gem", 'rdoc', 'coverage']

Rake::TestTask.new do |t|
  t.libs << 'test'
  t.pattern = %w( test/test_*.rb )
end

desc 'Build and package gem'
task default: :package

# Tests

desc 'Run tests'
task :test

# Gem Packaging and Release

desc "Packages #{NAME}"
task package: [ :clean ] do |p|
  sh %{gem build #{NAME}.gemspec}
end

desc "Publish #{NAME}"
task publish: [ :package ] do |p|
  sh %{gem push #{ NAME }-#{ MultiformatCV::VERSION }.gem}
end

# Documentation

YARD::Rake::YardocTask.new do |t|
  t.files = %w( lib/**/*.rb )
end

task :yard

### Other

desc 'Start an IRB shell using the extension'
task :irb do
  require 'rbconfig'
  ruby = ENV['RUBY'] || File.join(RbConfig::CONFIG['bindir'], RbConfig::CONFIG['ruby_install_name'])
  irb = ENV['IRB'] || File.join(RbConfig::CONFIG['bindir'], File.basename(ruby).sub('ruby', 'irb'))
  sh %{#{irb} -I lib -r #{NAME}}
end
