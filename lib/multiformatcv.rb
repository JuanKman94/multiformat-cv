require 'yaml'
require 'erubi'

module MultiformatCV

  # YAML files that will be read from the specified directory
  YML_FILES = %w( contact jobs personal )

  # Parse yaml files located at data directory
  #
  # @param [Hash] opts Options to specify format and templates source
  # @option opts [String] 'data_dir' Directory where the YAML files are located.
  # @option opts [Symbol] 'format' One of [ :html, :tex ]
  # @option opts [Symbol] 'output' Output file
  # @option opts [String] 'templates' Directory where templates are located,
  #   expected template files are `cv.#{ format }.erb`
  #
  # @return [MultiformatCV::Resume] Resume with parsed data
  # @see https://www.rubydoc.info/gems/erubi/1.8.0 Erubi documentation
  # @see Erubi::Engine

  def self.generate(opts = {})
    opts['format'] ||= 'html'
    opts['data_dir'] ||= 'data'
    opts['templates'] ||= default_template_path
    opts['output'] ||= nil

    template = File.read("#{ opts['templates'] }/cv.#{ opts['format'] }.erb")
    resume = Resume.new
    yml = nil

    YML_FILES.each do |f|
      yml = YAML.load_file("#{ opts['data_dir'] }/#{ f }.yml")
      if yml then
        resume.send("add_#{ f }", yml[f])
      end
    end

    resume.rendered = eval Erubi::Engine.new(template).src

    if opts['output'] then
      File.open(opts['output'], 'w') { |f| f.write(resume.rendered) }
    end

    resume
  end

  private

  def self.default_template_path
    File.expand_path('../../sample/onepager', __FILE__)
  end
end

require 'multiformatcv/contact'
require 'multiformatcv/job'
require 'multiformatcv/personal'
require 'multiformatcv/project'
require 'multiformatcv/resume'
require 'multiformatcv/version'
