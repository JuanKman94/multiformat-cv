class MultiformatCV::Job
  attr_accessor :position,
    :company,
    :start_date,
    :end_date,
    :summary

  # List of projects that you worked on during this job
  # @return [Array<MultiformatCV::Project>]
  attr_accessor :projects

  # Create Job instance
  #
  # == Example:
  #   job = MultiformatCV::Job.new(position: 'Accountant', company: 'Accountants Inc.')
  #
  # @param [Hash] h Instance initializer; keys *MUST* be strings
  # @option h [String] 'position'
  # @option h [String] 'company'
  # @option h [String] 'start_date'
  # @option h [String] 'end_date'
  # @option h [String] 'summary'
  # @option h [Array<Hash>] 'projects' List of hashes that will be used to
  #   initialize new MultiformatCV::Porject entries
  #
  # @see MultiformatCV::Project

  def initialize(h = {})
    @position = h['position']
    @company = h['company']
    @start_date = h['start_date']
    @end_date = h['end_date']
    @summary = h['summary']
    @projects = []

    if h['projects']
      h['projects'].each do |p|
        @projects << MultiformatCV::Project.new(p)
      end
    end
  end

  # @return [DateTime] Date or nil
  def end_date
    if @end_date and @end_date != ''
      return DateTime.parse(@end_date)
    end
    return nil
  end

  # @return [DateTime] Date or nil
  def start_date
    if @start_date and @start_date != ''
      return DateTime.parse(@start_date)
    end
    return nil
  end
end
