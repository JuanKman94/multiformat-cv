require 'date'

class MultiformatCV::Resume
  # Contact information
  # @return [MultiformatCV::Contact]
  attr_accessor :contact

  # List of jobs
  # @return [Array<MultiformatCV::Job>]
  attr_accessor :jobs

  # List of skills
  # @key    skill name
  # @value  time duration in months
  # @return [Hash]
  attr_accessor :skills

  # Personal accomplishments and projects
  # @return [MultiformatCV::Personal]
  attr_accessor :personal

  # Rendered templates with CV data
  # @return [String]
  attr_accessor :rendered

  # Initialize Resume
  #
  # @param [Hash] h Options
  # @option h [Array<MultiformatCV::Contact>] 'contact' Contact information
  # @option h [Array<MultiformatCV::Job>] 'jobs' Jobs list
  # @option h [Array<MultiformatCV::Personal>] 'personal' Personal information

  def initialize(h = {})
    @contact = MultiformatCV::Contact.new(h['contact'] || []) if h['contact']
    @jobs = []
    @skills = {}
    @personal = MultiformatCV::Personal.new(h['personal'] || []) if h['personal']

    h['jobs'].each { |j| @jobs << MultiformatCV::Job.new(j) } if h['jobs']

    estimate_skills_experience_duration! if jobs.length > 0
  end

  # Set contact information
  # @param [Hash] h Hash used to initialize new MultiformatCV::Contact
  # @see MultiformatCV::Contact

  def add_contact(h = {})
    @contact = MultiformatCV::Contact.new(h)
  end

  # Add list of job entries to @jobs list
  #
  # @param [Array<Hash>] arr List of hashes that will be used to initialize new
  #   MultiformatCV::Job entries
  #
  # @see MultiformatCV::Job

  def add_jobs(arr = [])
    arr.each { |j| @jobs << MultiformatCV::Job.new(j) }
    estimate_skills_experience_duration!
  end

  # Set personal information
  # @param [Hash] h Hash used to initialize new MultiformatCV::Personal
  # @see MultiformatCV::Personal

  def add_personal(h = {})
    @personal = MultiformatCV::Personal.new(h)
  end

  private

  # A skill can be shared among projects, so we need to **NOT** count the
  # same skill more than once if it appears in more than one.
  #
  # We do a naive calculation of _assuming_ if a skill has the same
  # duration in more than one project, then we count it only once.
  #
  # **NOTE**: the way dates are parsed, if they don't specify the day,
  # start of the month assumed; if they don't specify month, start of the
  # year is assumed.
  #
  # For example, this approach will give approximate results when
  # `skill1` was used from January 'til March in `project1` and from
  # June 'til August in `project2`, giving a total of 4 months
  # assuming dates are:
  #
  # * 2020-01-01 -> 2020-03-01: 2 months
  # * 2020-06-01 -> 2020-08-01: 2 months

  def estimate_skills_experience_duration!
    jobs.each do |job|
      job_skills = {}
      default_start_date = job.start_date
      default_end_date   = job.end_date || Time.now # present/current job

      job.projects.each do |proj|
        start_date = proj.start_date || default_start_date
        end_date   = proj.end_date   || default_end_date

        proj.skills.each do |s|
          job_skills[s] ||= []
          job_skills[s] << diff_in_months(start_date.to_time, end_date.to_time)
        end
      end

      # we assume that if the same skill has the same duration twice for the
      # same job, the projects were developed in parallel so we count the skill
      # only once
      job_skills.each { |s, arr| job_skills[s] = arr.uniq.sum }
      skills.merge!(job_skills) { |k, oldval, newval| oldval + newval }
    end

    skills
  end

  # Generic (30-day) month duration in seconds
  ONE_MONTH = 3600 * 24 * 30

  # @param [Time] t1
  # @param [Time] t2
  # @return [Integer] Difference in months between dates
  def diff_in_months(t1, t2)
    ((t1 - t2) / ONE_MONTH).abs.round
  end
end
