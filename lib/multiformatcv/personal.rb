class MultiformatCV::Personal
  attr_accessor :summary

  # List of personal projects
  # @return [Array<MultiformatCV::Project>]
  attr_accessor :projects

  # Create Personal information instance
  #
  # == Example:
  #   personal = MultiformatCV::Personal.new(summary: 'Volunteer at...', projects: [])
  #
  # @param [Hash] h Instance initializer; keys *MUST* be strings
  # @option h [String] 'summary'
  # @option h [Array<Hash>] 'projects' List of hashes that will be used to
  #   initialize new MultiformatCV::Porject entries
  #
  # @see MultiformatCV::Project

  def initialize(h = {})
    @summary = h['summary']
    @projects = []

    h['projects'].each { |p| @projects << MultiformatCV::Project.new(p) }
  end
end
