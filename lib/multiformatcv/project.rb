class MultiformatCV::Project
  attr_accessor :name,
    :position,
    :summary,
    :tasks,
    :skills

  # Repository URL for the project
  attr_accessor :repo

  # Page or site URL for the project
  attr_accessor :url

  # Create Project instance
  #
  # == Example:
  #   project = MultiformatCV::Project.new(name: 'Life Solver (TM)', summary: '...', ...)
  #
  # @param [Hash] h Instance initializer; keys *MUST* be strings
  # @option h [String] 'name'
  # @option h [String] 'position'
  # @option h [String] 'start_date'
  # @option h [String] 'end_date'
  # @option h [String] 'repo' Repository URL
  # @option h [String] 'url' URL for the project
  # @option h [String] 'summary' Project description, goals, etc.
  # @option h [Array<String>] 'tasks' Tasks done for the project
  # @option h [Array<String>] 'skills' skills used in the project

  def initialize(h = {})
    @name = h['name']
    @position = h['position']
    @start_date = h['start_date']
    @end_date = h['end_date']
    @repo = h['repo']
    @summary = h['summary']
    @url = h['url']
    @tasks = []
    @skills = []

    h['tasks'].each  { |t| @tasks << t } if h['tasks']
    h['skills'].each { |t| @skills << t } if h['skills']
  end

  # @return [DateTime] Date or nil
  def end_date
    if @end_date and @end_date != ''
      return DateTime.parse(@end_date)
    end
    return nil
  end

  # @return [DateTime] Date or nil
  def start_date
    if @start_date and @start_date != ''
      return DateTime.parse(@start_date)
    end
    return nil
  end
end
