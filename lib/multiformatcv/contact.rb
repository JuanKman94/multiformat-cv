class MultiformatCV::Contact
  attr_accessor :name,
    :title,
    :phone,
    :email

  # Hash of links
  #
  # == Example:
  #   links = { github: 'https://www.github.com/john.doe/personal-dashboard' }
  attr_accessor :links

  # Create Contact instance
  #
  # == Example:
  #   contact = MultiformatCV::Contact.new(title: 'Accountant', name: 'John Doe')
  #
  # @param [Hash] h Instance initializer; keys *MUST* be strings
  # @option h [String] 'name'
  # @option h [String] 'title'
  # @option h [String] 'email'
  # @option h [String] 'phone'
  # @option h [Hash<String, String>] 'links'

  def initialize(h = {})
    @name  = h['name']
    @title = h['title']
    @email = h['email']
    @phone = h['phone']
    @links = h['links']
  end
end
