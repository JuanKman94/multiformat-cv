# Multiformat CV

Multi-format CV generator from YAML files and ERB templates.

## Install

```bash
$ gem install multiformat-cv
```

## Usage

Taking this repository's `sample` directory as a starting point, we can
generate the CV as follows:

```bash
$ tree
.
├── data
│   ├── contact.yml
│   ├── jobs.yml
│   └── personal.yml
└── onepager
    ├── cv.css
    └── cv.html.erb

2 directories, 5 files
$ multiformatcv -d sample/data -s sample/onepager -o sample/onepager/cv.html
$ open sample/onepager/cv.html
```

Which, after being printed from the browser, would look something like this:

![Printed CV](sample/onepager/images/cv.png)
