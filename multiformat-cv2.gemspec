# frozen_string_literal: true

lib = File.expand_path("lib", __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "multiformatcv/version"

Gem::Specification.new do |s|
  s.name          = "multiformat-cv2"
  s.version       = MultiformatCV::VERSION
  s.platform      = Gem::Platform::RUBY
  s.summary       = "Generate your CV in different formats"
  s.description   = "Multi-format CV generator from YAML files"
  s.authors       = [ "juankman94" ]
  s.email         = "juan.carlos@hey.com"
  s.homepage      = "https://www.gitlab.com/juankman94/multiformat-cv"
  s.license       = "MIT"

  s.files         = Dir[ "README.md", "MIT-LICENSE", "bin/**/*", "lib/**/*.rb", "lib/**/*.erb" ]
  s.require_paths = [ "lib" ]

  s.bindir        = "bin"
  s.executables   = [ "multiformatcv" ]

  s.add_runtime_dependency "erubi",     "~> 1.8"
  s.add_runtime_dependency "mercenary", "~> 0.3.3"
end
